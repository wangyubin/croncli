package main

import (
	"fmt"
)

type CmdJob struct {
	OsType string
	Args   []string
}

func NewCmdJob(osType string, args []string) CmdJob {
	return CmdJob{
		OsType: osType,
		Args:   args,
	}
}

func (j CmdJob) Run() {
	output, err := ExecCmd(j.OsType, j.Args)
	if err != nil {
		panic(err)
	}

	fmt.Printf("OUTPUT>>>>>\n%s\n", output)
}
